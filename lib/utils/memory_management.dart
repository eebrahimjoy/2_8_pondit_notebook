import 'package:shared_preferences/shared_preferences.dart';

class MemoryManagement {
  static final String IS_LOGGED = 'logged-in';
  static final String NAME = 'name';
  static SharedPreferences? prefs;

  static Future<bool> init() async {
    prefs = await SharedPreferences.getInstance();
    return true;
  }

  static void setIsLoggedIn({required bool isLogged}) {
    prefs!.setBool(IS_LOGGED, isLogged);
  }

  static bool getIsLoggedIn() {
    bool? value = prefs!.getBool(IS_LOGGED);
    return value!;
  }

  static void setName({required String name}) {
    prefs!.setString(NAME, name);
  }

  static String getName() {
    String? value = prefs!.getString(NAME);
    return value!;
  }
}
