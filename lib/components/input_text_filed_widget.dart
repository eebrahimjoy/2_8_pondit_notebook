import 'package:flutter/material.dart';
import 'package:notebook/utils/app_colors.dart';
import 'package:notebook/utils/validator_functions.dart';

class InputTextFiledWidget extends StatelessWidget {
  final TextEditingController? textEditorCtrl;
  final TextInputType? keyboardType;
  final String? hint;
  final String? validText;
  final bool? isName;

  InputTextFiledWidget(
      {@required this.textEditorCtrl,
      @required this.keyboardType,
      @required this.hint,
      @required this.validText,
      this.isName});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 55,
      alignment: Alignment.centerLeft,
      child: TextFormField(
        keyboardType: keyboardType,
        obscureText: false,
        controller: textEditorCtrl,
        enabled: true,
        decoration: InputDecoration(
          contentPadding:
              const EdgeInsets.only(left: 11, right: 3, top: 14, bottom: 14),
          errorStyle: const TextStyle(fontSize: 11, height: 0.3),
          errorBorder: const OutlineInputBorder(
            borderSide: BorderSide(
              width: .5,
              color: Colors.red,
            ),
          ),
          enabledBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(4)),
            borderSide: BorderSide(color: Colors.black, width: 0.5),
          ),
          focusedErrorBorder: const OutlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: Colors.red,
            ),
          ),
          focusedBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(4)),
            borderSide: BorderSide(color: Colors.black, width: 0.5),
          ),
          hintText: hint,
          hintStyle: const TextStyle(
            fontSize: 16,
            color: Color(0xffbcbcbc),
          ),
        ),
        style: const TextStyle(
          fontSize: 16,
          color: Colors.black,
        ),
        cursorColor: AppColors.qColorBlue,
        cursorWidth: 1,
        validator: (value) {
          if (isName == true) {
            if (value == null || value.isEmpty) {
              return validText;
            }
            return null;
          } else {
            if (value == null ||
                value.isEmpty ||
                isEmailFormatNotValid(textEditorCtrl!.text) == true) {
              return validText;
            }
            return null;
          }
        },
      ),
    );
  }
}
