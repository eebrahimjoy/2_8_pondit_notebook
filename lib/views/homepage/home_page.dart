import 'package:flutter/material.dart';
import 'package:notebook/database/database_helper.dart';
import 'package:notebook/models/user.dart';
import 'package:notebook/provider/notebook_provider.dart';
import 'package:notebook/views/drawer/drawer_page.dart';
import 'package:notebook/models/notebook.dart';
import 'package:notebook/views/note_add_page/note_add_page.dart';
import 'package:notebook/utils/app_colors.dart';
import 'package:notebook/utils/custom_toast.dart';
import 'package:notebook/views/note_update_page/note_update_page.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  final String name;
  HomePage({required this.name});
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String name = 'XYZ';
  String? _greeting;
  String? noData;

  @override
  void initState() {
    super.initState();
    noData = "No note available, add new";
    greetings();
    NotebookProvider notebookProvider =
        Provider.of<NotebookProvider>(context, listen: false);
    notebookProvider.isLoading = true;
    notebookProvider.fetchNoteList();
    ;
  }

  void greetings() {
    var timeOfDay = DateTime.now().hour;
    if (timeOfDay >= 0 && timeOfDay < 6) {
      _greeting = 'Good Night';
    } else if (timeOfDay >= 0 && timeOfDay < 12) {
      _greeting = 'Good Morning';
    } else if (timeOfDay >= 12 && timeOfDay < 16) {
      _greeting = 'Good Afternoon';
    } else if (timeOfDay >= 16 && timeOfDay < 21) {
      _greeting = 'Good Evening';
    } else if (timeOfDay >= 21 && timeOfDay < 24) {
      _greeting = 'Good Night';
    }
  }

  Future<void> showMenuSelection(String value, NotebookProvider provider,
      int id, NoteBook mNotebook) async {
    switch (value) {
      case 'update':
        bool isUpdated = await Navigator.push(
          context,
          MaterialPageRoute(builder: (context) {
            return NoteUpdatePage(
              notebook: mNotebook,
            );
          }),
        );

        if ((isUpdated == true)) {
          provider.fetchNoteList();
        }

        break;

      case 'delete':
        String status = await provider.deleteNote(id);
        if (status == "success") {
          CustomToast.toast("Note has been successfully deleted");
          provider.fetchNoteList();
        } else {
          CustomToast.toast("Delete failed");
        }

        break;
    }
  }

  void filterSearchResult(String query, NotebookProvider provider) {
    provider.noteList = [];
    if (query.isNotEmpty) {
      List<NoteBook> newList = [];
      for (NoteBook noteBook in provider.noteFilterList) {
        if (noteBook.title!.toLowerCase().contains(query.toLowerCase())) {
          newList.add(noteBook);
        }
      }
      provider.noteList = newList;
    } else {
      provider.noteList = provider.noteFilterList;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<NotebookProvider>(
      builder: (_, provider, ___) {
        return Scaffold(
            floatingActionButton: Container(
              margin: const EdgeInsets.only(right: 20, bottom: 20),
              child: FloatingActionButton(
                  elevation: 0.0,
                  child: Icon(Icons.add),
                  backgroundColor: AppColors.qColorPrimary,
                  onPressed: () async {
                    bool isAdded = await Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return NoteAddPage();
                      }),
                    );
                    if (isAdded == true) {
                      provider.fetchNoteList();
                    }
                  }),
            ),
            body: Scaffold(
              appBar: AppBar(
                title: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text.rich(
                      TextSpan(
                        children: [
                          const TextSpan(
                            text: 'keep',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 25,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          TextSpan(
                            text: 'Note',
                            style: TextStyle(
                              color: Colors.grey[400],
                              fontSize: 30,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              drawer: DrawerPage(),
              body: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            const Icon(
                              Icons.menu_book,
                              size: 40,
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Text(
                              'Hello, ${widget.name}' ,
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6!
                                  .copyWith(
                                    fontWeight: FontWeight.w400,
                                  ),
                            ),
                            _greeting != null
                                ? Text(
                                    _greeting!,
                                    style: const TextStyle(
                                      color: Colors.grey,
                                      fontSize: 15,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  )
                                : Container(),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            const SizedBox(
                              height: 25,
                            ),
                            Container(
                              padding: const EdgeInsets.only(
                                  top: 0, bottom: 0, left: 15, right: 15),
                              height: 55,
                              child: TextField(
                                onChanged: (value) {
                                  print(value);
                                  filterSearchResult(value, provider);
                                },
                                // controller: _editingController,
                                decoration: const InputDecoration(
                                  labelText: 'search by title...',
                                  prefixIcon: Icon(Icons.search),
                                  fillColor: AppColors.qColorLight,
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(color: Colors.white),
                                  ),
                                  border: UnderlineInputBorder(
                                    borderSide: BorderSide(color: Colors.white),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(8.0)),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(8.0)),
                                      borderSide: BorderSide(
                                          color: AppColors.qColorPrimary)),
                                  filled: true,
                                  contentPadding: EdgeInsets.only(
                                      bottom: 10.0, left: 10.0, right: 10.0),
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            provider.isLoading == false
                                ? provider.noteList.isEmpty
                                    ? const Center(
                                        child:
                                            Text("No note available, add new"))
                                    : ListView.separated(
                                        separatorBuilder: (context, index) =>
                                            const SizedBox(
                                          height: 5,
                                        ),
                                        shrinkWrap: true,
                                        physics:
                                            const NeverScrollableScrollPhysics(),
                                        itemCount: provider.noteList.length,
                                        itemBuilder: (context, index) {
                                          return noteListItem(
                                              provider.noteList[index],
                                              provider);
                                        },
                                      )
                                : const Center(
                                    child: CircularProgressIndicator(
                                      valueColor: AlwaysStoppedAnimation<Color>(
                                          AppColors.qColorPrimary),
                                    ),
                                  )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ));
      },
    );
  }

  Widget noteListItem(NoteBook noteBook, NotebookProvider provider) {
    return Padding(
      padding: const EdgeInsets.only(left: 15, right: 15, top: 0, bottom: 0),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(
            color: AppColors.qColorBlue2ndPrimary,
          ),
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      noteBook.title!,
                      style: Theme.of(context)
                          .textTheme
                          .subtitle1!
                          .copyWith(fontWeight: FontWeight.w700),
                    ),
                    Text(
                      noteBook.content!,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context).textTheme.subtitle2,
                    ),
                    Text(
                      noteBook.date!,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context).textTheme.bodyText2,
                    ),
                  ],
                ),
              ),
              PopupMenuButton<String>(
                padding: EdgeInsets.zero,
                icon: Icon(Icons.more_vert),
                onSelected: (value) {
                  showMenuSelection(value, provider, noteBook.id, noteBook);
                },
                itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
                  const PopupMenuItem<String>(
                      value: 'update',
                      child: ListTile(
                          leading: Icon(Icons.edit), title: Text('Update'))),
                  const PopupMenuItem<String>(
                      value: 'delete',
                      child: ListTile(
                          leading: Icon(Icons.delete), title: Text('Delete'))),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
