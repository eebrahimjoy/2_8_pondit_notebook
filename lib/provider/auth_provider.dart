import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:notebook/models/user.dart';
import 'package:notebook/repository/notebook_repository.dart';

class AuthProvider with ChangeNotifier {
  BuildContext? _context;

  bool? _isLoading;

  BuildContext get context => _context!;

  getHomeContext() => _context;

  void setHomeContext(BuildContext context) {
    _context = context;
  }

  //saveUser
  Future<String> saveUser(User user) async {
    String status = "loading";
    isLoading = true;
    notifyListeners();
    int value = await NoteBookRepository().saveUser(user);
    if (value == 1) {
      status = "success";
    } else {
      status = "error";
    }
    isLoading = false;
    notifyListeners();
    return status;
  }

  //isUserExist
  Future<bool> isUserExist(String email) async {
    bool status = false;
    isLoading = true;
    notifyListeners();
    bool value = await NoteBookRepository().isUserExist(email);

    print(value);
    if (value == true) {
      status = true;
    }
    isLoading = false;
    notifyListeners();
    return status;
  }

  //loginUser
  Future<List<User>> loginUser(String email,String password) async {
    String status;
    isLoading = true;
    notifyListeners();
    List<User> value =
        await NoteBookRepository().loginUser(email, password);

    isLoading = false;
    notifyListeners();
    return value;
  }

  bool get isLoading => _isLoading!;

  set isLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }
}
