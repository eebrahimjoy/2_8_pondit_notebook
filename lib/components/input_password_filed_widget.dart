import 'package:flutter/material.dart';
import 'package:notebook/utils/app_colors.dart';

class InputPasswordFiledWidget extends StatelessWidget {
  final TextEditingController? textEditorCtrl;
  final TextInputType? keyboardType;
  final String? hint;
  final String? validText;

  InputPasswordFiledWidget(
      {this.textEditorCtrl, this.keyboardType, this.hint, this.validText});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 55,
      alignment: Alignment.centerLeft,
      child: TextFormField(
        keyboardType: TextInputType.text,
        controller: textEditorCtrl,
        obscureText: true,
        enabled: true,
        decoration: InputDecoration(
          contentPadding:
              const EdgeInsets.only(left: 11, right: 3, top: 14, bottom: 14),
          errorStyle: const TextStyle(fontSize: 11, height: 0.3),
          errorBorder: const OutlineInputBorder(
            borderSide: BorderSide(
              width: .5,
              color: Colors.red,
            ),
          ),
          enabledBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(4)),
            borderSide: BorderSide(color: Colors.black, width: 0.5),
          ),
          focusedErrorBorder: const OutlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: Colors.red,
            ),
          ),
          focusedBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(4)),
            borderSide: BorderSide(color: Colors.black, width: 0.5),
          ),
          hintText: hint,
          hintStyle: TextStyle(
            fontSize: 16,
            color: Color(0xffbcbcbc),
          ),
        ),
        style: TextStyle(
          fontSize: 16,
          color: Colors.black,
        ),
        cursorColor: AppColors.qColorBlue,
        cursorWidth: 1,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return validText;
          } else if (textEditorCtrl!.text.length < 8) {
            return "Password must be more than 7 char...";
          } else {
            return null;
          }
        },
      ),
    );
  }
}
