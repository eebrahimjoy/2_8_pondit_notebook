import 'package:flutter/cupertino.dart';
import 'package:notebook/models/notebook.dart';
import 'package:notebook/repository/notebook_repository.dart';

class NotebookProvider with ChangeNotifier {
  bool? _isLoading;
  List<NoteBook> _noteList = [];
  List<NoteBook> _noteFilterList = [];

  //insertNote
  Future<String> insertNote(NoteBook noteBook) async {
    String status = "loading";
    isLoading = true;
    notifyListeners();
    int value = await NoteBookRepository().insertNote(noteBook);
    if (value == 1) {
      status = "success";
    } else {
      status = "error";
    }
    isLoading = false;
    notifyListeners();
    return status;
  }

  //fetchNoteList
  Future<void> fetchNoteList() async {
    isLoading = true;
    notifyListeners();
    _noteList = await NoteBookRepository().fetchNoteList();
    _noteFilterList = await NoteBookRepository().fetchNoteList();
    isLoading = false;
    notifyListeners();
  }

  //deleteNote
  Future<String> deleteNote(int id) async {
    String status = "loading";
    isLoading = true;
    notifyListeners();
    int value = await NoteBookRepository().deleteNote(id);
    if (value == 1) {
      status = "success";
    } else {
      status = "error";
    }
    isLoading = false;
    notifyListeners();
    return status;
  }

  //deleteTable
  Future<String> deleteTable(String tableName) async {
    String status = "loading";
    isLoading = true;
    notifyListeners();
    int value = await NoteBookRepository().deleteTable(tableName);
    if (value == 1) {
      status = "success";
    } else {
      status = "error";
    }
    isLoading = false;
    notifyListeners();
    return status;
  }

  //updateNote
  Future<String> updateNote(NoteBook noteBook) async {
    String status = "loading";
    isLoading = true;
    notifyListeners();
    int value = await NoteBookRepository().updateNote(noteBook);
    if (value == 1) {
      status = "success";
    } else {
      status = "error";
    }
    isLoading = false;
    notifyListeners();
    return status;
  }

  bool get isLoading => _isLoading!;

  set isLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }

  List<NoteBook> get noteList => _noteList;


  set noteList(List<NoteBook> value) {
    _noteList = value;
    notifyListeners();
  }

  List<NoteBook> get noteFilterList => _noteFilterList;

  set noteFilterList(List<NoteBook> value) {
    _noteFilterList = value;
    notifyListeners();
  }
}
