import 'package:flutter/material.dart';
import 'package:notebook/components/custom_button.dart';
import 'package:notebook/components/input_password_filed_widget.dart';
import 'package:notebook/components/input_text_filed_widget.dart';
import 'package:notebook/models/user.dart';
import 'package:notebook/provider/auth_provider.dart';
import 'package:notebook/utils/app_colors.dart';
import 'package:notebook/utils/custom_toast.dart';
import 'package:notebook/views/authentication/login_page.dart';
import 'package:provider/provider.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  GlobalKey<FormState>? _formKey;
  TextEditingController? _nameController;
  TextEditingController? _emailController;
  TextEditingController? _passwordController;
  TextEditingController? _confirmPasswordController;

  @override
  void initState() {
    super.initState();
    _formKey = GlobalKey<FormState>();
    _nameController = TextEditingController();
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
    _confirmPasswordController = TextEditingController();
    AuthProvider authProvider =
        Provider.of<AuthProvider>(context, listen: false);
    authProvider.isLoading = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.qColorLight,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 38, right: 38),
            child: Consumer<AuthProvider>(
              builder: (_, provider, __) {
                return Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      InputTextFiledWidget(
                        isName: true,
                        textEditorCtrl: _nameController,
                        keyboardType: TextInputType.text,
                        hint: "Enter your name",
                        validText: "Entre your valid name",
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      InputTextFiledWidget(
                        textEditorCtrl: _emailController,
                        keyboardType: TextInputType.emailAddress,
                        hint: "Enter your email",
                        validText: "Enter your valid email",
                        isName: false,
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      InputPasswordFiledWidget(
                        textEditorCtrl: _passwordController,
                        hint: "Enter your password",
                        validText: "Enter your valid password",
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      InputPasswordFiledWidget(
                        textEditorCtrl: _confirmPasswordController,
                        hint: "Confirm password",
                        validText: "Confirm password not valid",
                      ),
                      const SizedBox(
                        height: 35,
                      ),
                      provider.isLoading==false
                          ? CustomButton(
                              onPressed: () async {
                                if (_formKey!.currentState!.validate()) {
                                  if (_passwordController!.text ==
                                      _confirmPasswordController!.text) {
                                    User user = User(
                                        name:_nameController!.text,
                                        username:_emailController!.text,
                                       password: _passwordController!.text);

                                    bool status = await provider
                                        .isUserExist(_emailController!.text);

                                    if (status == true) {
                                      CustomToast.toast(
                                          "Username already exist");
                                    } else {
                                      String status =
                                          await provider.saveUser(user);

                                      if (status == "success") {
                                        CustomToast.toast(
                                            "Registration success");

                                        Navigator.of(context)
                                            .pushAndRemoveUntil(
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        LoginPage()),
                                                (route) => false);
                                      } else {
                                        CustomToast.toast(
                                            "Registration failed");
                                      }
                                    }
                                  } else {
                                    CustomToast.toast("Password not same");
                                  }
                                }
                              },
                        color: AppColors.qColorBlue,
                              text: "REGISTER",
                            )
                          : const Center(
                              child: CircularProgressIndicator(),
                            ),
                      const SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Text(
                              "Already registered?",
                            ),
                            InkWell(
                              borderRadius: BorderRadius.circular(2),
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                              child: const Padding(
                                padding: EdgeInsets.all(5),
                                child: Text(
                                  "LOGIN",
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
