import 'package:flutter/material.dart';
import 'package:notebook/components/custom_button.dart';
import 'package:notebook/components/input_password_filed_widget.dart';
import 'package:notebook/components/input_text_filed_widget.dart';
import 'package:notebook/models/user.dart';
import 'package:notebook/provider/auth_provider.dart';
import 'package:notebook/utils/app_colors.dart';
import 'package:notebook/utils/custom_toast.dart';
import 'package:notebook/utils/memory_management.dart';
import 'package:notebook/views/authentication/register_page.dart';
import 'package:notebook/views/homepage/home_page.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageWidgetState createState() => _LoginPageWidgetState();
}

class _LoginPageWidgetState extends State<LoginPage> {
  TextEditingController? _emailController;
  TextEditingController? _passwordController;
  GlobalKey<FormState>? _formKey;

  @override
  void initState() {
    super.initState();
    _formKey = GlobalKey<FormState>();
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
    AuthProvider authProvider =
        Provider.of<AuthProvider>(context, listen: false);
    authProvider.isLoading = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.qColorLight,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 38, right: 38),
              child: Consumer<AuthProvider>(
                builder: (_, provider, __) {
                  return Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        InputTextFiledWidget(
                          isName: true,
                          textEditorCtrl: _emailController,
                          keyboardType: TextInputType.emailAddress,
                          hint: "Enter email",
                          validText: "Enter your valid email",
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        InputPasswordFiledWidget(
                            textEditorCtrl: _passwordController,
                            hint: "Enter your password",
                            validText: "Enter your correct password"),
                        provider.isLoading == false
                            ? CustomButton(
                                onPressed: () async {
                                  if (_formKey!.currentState!.validate()) {
                                    List<User> status =
                                        await provider.loginUser(
                                            _emailController!.text,
                                            _passwordController!.text);
                                    if (status.isNotEmpty) {
                                      CustomToast.toast("Login success");

                                      MemoryManagement.setIsLoggedIn(
                                          isLogged: true);

                                      MemoryManagement.setName(
                                          name: status[0].name);

                                      Navigator.of(context).pushAndRemoveUntil(
                                          MaterialPageRoute(
                                              builder: (context) => HomePage(
                                                    name: status[0].name,
                                                  )),
                                          (route) => false);
                                    } else {
                                      CustomToast.toast(
                                          "Login failed, username or password error");
                                    }
                                  }
                                },
                                color: AppColors.qColorBlue,
                                text: "LOGIN",
                              )
                            : const Center(
                                child: CircularProgressIndicator(),
                              ),
                        SafeArea(
                          child: Center(
                            child: Wrap(
                              children: [
                                const Padding(
                                  padding: EdgeInsets.only(
                                      bottom: 32, top: 0, left: 5, right: 5),
                                  child: Text(
                                    "New to app",
                                  ),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) {
                                        return RegisterPage();
                                      }),
                                    );
                                  },
                                  child: const Text(
                                    "Register",
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        )
                      ],
                    ),
                  );
                },
              ),
            ),
          ],
        ));
  }
}
