import 'dart:io';
import 'package:notebook/models/notebook.dart';
import 'package:notebook/models/user.dart';
import 'package:notebook/utils/app_constant.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DatabaseHelper {
  Future<Database> initDatabase() async {
    Directory directory = await getApplicationDocumentsDirectory();
    final path = join(directory.path, 'notebook.db');
    return openDatabase(
      path,
      version: 1,
      onCreate: (Database db, int version) async {
        await db.execute("CREATE TABLE " +
            AppConstants.NOTE_TABLE +
            "(id INTEGER PRIMARY KEY AUTOINCREMENT,title TEXT,content TEXT,date TEXT)"
                "");

        await db.execute("CREATE TABLE " +
            AppConstants.USER_TABLE +
            "(id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT,username TEXT,password TEXT)"
                "");
      },
    );
  }

  Future<int> insertNote(NoteBook noteBook) async {
    final db = await initDatabase();
    return db.insert(AppConstants.NOTE_TABLE, noteBook.toMap(),
        conflictAlgorithm: ConflictAlgorithm.ignore);
  }

  Future<bool> isUserExist(String userName) async {
    final db = await initDatabase();
    final mUser =
        await db.rawQuery("SELECT * FROM user WHERE username = '$userName'");

    List<User> users = List.generate(mUser.length, (index) {
      return User(
        id: mUser[index]['id'],
        name: mUser[index]['name'],
        username: mUser[index]['username'],
        password: mUser[index]['password'],
      );
    });

    if (users.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  }

  Future<List<User>> loginUser(String userName, String password) async {
    final db = await initDatabase();
    final mUser = await db.rawQuery(
        "SELECT * FROM user WHERE username = '$userName' and password = '$password'");

    List<User> users = List.generate(mUser.length, (index) {
      return User(
        id: mUser[index]['id'],
        name: mUser[index]['name'],
        username: mUser[index]['username'],
        password: mUser[index]['password'],
      );
    });

    return users;
  }

  Future<int> saveUser(User user) async {
    final db = await initDatabase();
    return db.insert(AppConstants.USER_TABLE, user.toMap());
  }

  Future<List<NoteBook>> fetchNoteList() async {
    final db = await initDatabase();
    final maps = await db.query(AppConstants.NOTE_TABLE);
    return List.generate(maps.length, (index) {
      return NoteBook(
        id: maps[index]['id'],
        title: maps[index]['title'],
        content: maps[index]['content'],
        date: maps[index]['date'],
      );
    });
  }

  Future<int> updateNote(NoteBook noteBook) async {
    final db = await initDatabase();
    return db.update(AppConstants.NOTE_TABLE, noteBook.toMap(),
        where: "id = ?", whereArgs: [noteBook.id]);
  }

  Future<int> deleteNote(int mId) async {
    final db = await initDatabase();
    return db
        .delete(AppConstants.NOTE_TABLE, where: "id = ?", whereArgs: [mId]);
  }

  Future<int> deleteTable(String tableName) async {
    final db = await initDatabase();
    return db.delete(tableName);
  }
}
