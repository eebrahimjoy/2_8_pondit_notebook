class User {
  dynamic id;
  dynamic name;
  dynamic username;
  dynamic password;


  User({this.id, this.name, this.username, this.password});


  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map['name'] = name;
    map['username'] = username;
    map['password'] = password;
    return map;
  }
}
