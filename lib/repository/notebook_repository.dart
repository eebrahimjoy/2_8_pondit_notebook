import 'package:notebook/database/database_helper.dart';
import 'package:notebook/models/notebook.dart';
import 'package:notebook/models/user.dart';

class NoteBookRepository {
  //insertNote
  Future<int> insertNote(NoteBook noteBook) async{
    return DatabaseHelper().insertNote(noteBook);
  }

  //saveUser
  Future<int> saveUser(User user) async{
    return DatabaseHelper().saveUser(user);
  }

  //isUserExist
  Future<bool> isUserExist(String email) async {
    return DatabaseHelper().isUserExist(email);
  }

  //loginUser
  Future<List<User>> loginUser(String email, String password) async {
    return DatabaseHelper().loginUser(email,password);
  }

  //fetchNoteList
  Future<List<NoteBook>> fetchNoteList() async{
    return DatabaseHelper().fetchNoteList();
  }

  //deleteNote
  Future<int> deleteNote(int id) async{
    return DatabaseHelper().deleteNote(id);
  }

  //deleteTable
  Future<int> deleteTable(String tableName) async{
    return DatabaseHelper().deleteTable(tableName);
  }

  //updateNote
  Future<int> updateNote(NoteBook noteBook) async {
    return DatabaseHelper().updateNote(noteBook);
  }
}
